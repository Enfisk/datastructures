#pragma once
#include <initializer_list>
#include <iostream>
#include <list>

template <typename T>
class BinarySearchTree {
	template <typename U>
	struct Node {
		Node(U p_data, Node *p_parent) { m_data = p_data; m_parent = p_parent; };

		U m_data;
		Node *m_parent, *m_greater = nullptr, *m_lesser = nullptr;
	};

public:
	BinarySearchTree();
	BinarySearchTree(std::initializer_list<T> &p_values);
	//BinarySearchTree(const BinarySearchTree &p_other);
	~BinarySearchTree();

	void insert(const T &p_value);
	void erase(const T &p_value);
	void clear();

	unsigned int size() const;

	void traversal_pre_order();
	void traversal_in_order();
	void traversal_post_order();

private:
	Node<T>* search(const T &p_value) const;

	void erase_NoChildren(Node<T>* p_node);
	void erase_OneChild(Node<T>* p_node);
	void erase_TwoChildren(Node<T>* p_node);

	void traversal_pre_order(Node<T> *p_node);
	void traversal_in_order(Node<T> *p_node);
	void traversal_post_order(Node<T> *p_node);

	Node<T> *m_root;
	unsigned int m_size;
	std::list<T> m_valueList;
};

/********METHODS*********
*************************
*************************
************************/

template <typename T>
BinarySearchTree<T>::BinarySearchTree() {
	m_root = nullptr;
	m_size = 0;
}

template <typename T>
BinarySearchTree<T>::BinarySearchTree(std::initializer_list<T> &p_values) {
	m_root = nullptr;

	for (auto i = p_values.begin(); i != p_values.end(); ++i) {
		insert(*i);
	}
}
//
//template <typename T>
//BinarySearchTree<T>::BinarySearchTree(const BinarySearchTree &p_other) {
//
//}

template <typename T>
BinarySearchTree<T>::~BinarySearchTree() {
	clear();
	//cleaned up
}

template <typename T>
void BinarySearchTree<T>::insert(const T &p_value) {
	if (m_root == nullptr) {
		m_root = new Node<T>(p_value, nullptr);
		m_size++;
		return;
	}

	if (search(p_value) != nullptr) {
		std::cerr << p_value << " already exists!" << std::endl;
		return;
	}

	Node<T> *current = m_root, *previous = m_root;
	while (current != nullptr) {
		previous = current;

		if (p_value > current->m_data) {
			current = current->m_greater;
		}
		else {
			current = current->m_lesser;
		}
	}

	current = new Node<T>(p_value, previous);
	(p_value > previous->m_data ? previous->m_greater : previous->m_lesser) = current;
	m_size++;
}

template <typename T>
void BinarySearchTree<T>::erase(const T &p_value) {		//This should no longer crash if you try to delete root.
	Node<T>* node = search(p_value);
	if (node == nullptr) {		//Can't be found
		std::cerr << "Can't find " << p_value << " in tree." << std::endl;
		return;
	}

	if (node->m_lesser == nullptr && node->m_greater == nullptr) {			//No children
		erase_NoChildren(node);
	}

	else if ((node->m_lesser != nullptr && node->m_greater == nullptr) || (node->m_lesser == nullptr && node->m_greater != nullptr)) {	//One child
		erase_OneChild(node);
	}

	else if (node->m_lesser && node->m_greater) {		//Two children
		erase_TwoChildren(node);
	}
}

template <typename T>
unsigned int BinarySearchTree<T>::size() const {
	return m_size;
}

template <typename T>
void BinarySearchTree<T>::traversal_pre_order() {
	traversal_pre_order(m_root);
	auto it = m_valueList.begin();
	while (it != m_valueList.end()) {
		std::cout << (*it) << std::endl;
		++it;
	}

	std::cout << std::endl;
	m_valueList.clear();
	//print -> left -> right
}

template <typename T>
void BinarySearchTree<T>::traversal_in_order() {
	traversal_in_order(m_root);
	auto it = m_valueList.begin();
	while (it != m_valueList.end()) {
		std::cout << (*it) << std::endl;
		++it;
	}

	std::cout << std::endl;
	m_valueList.clear();
	//left -> print -> right
}

template <typename T>
void BinarySearchTree<T>::traversal_post_order()  {
	traversal_post_order(m_root);
	auto it = m_valueList.begin();
	while (it != m_valueList.end()) {
		std::cout << (*it) << std::endl;
		++it;
	}

	std::cout << std::endl;
	m_valueList.clear();
	//left -> right -> print
}

template <typename T>
typename BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::search(const T &p_value) const {
	Node<T> *current = m_root, *previous = m_root;

	while (current != nullptr) {
		if (current->m_data == p_value) {
			break;
		}
		else {
			if (p_value > current->m_data) {
				previous = current;
				current = current->m_greater;
			}
			else {
				previous = current;
				current = current->m_lesser;
			}
		}
	}

	return current;
}

template <typename T>
void BinarySearchTree<T>::clear() {
	while (m_root != nullptr) {
		erase(m_root->m_data);
	}
}

template <typename T>
void BinarySearchTree<T>::erase_NoChildren(Node<T>* p_node) {
	if (p_node->m_parent == nullptr) {				//if root
		delete p_node;
		m_root = nullptr;
		m_size--;
		return;
	}

	(p_node->m_parent->m_greater == p_node ? p_node->m_parent->m_greater : p_node->m_parent->m_lesser) = nullptr;		//Determine in which direction it was linked, set to null

	delete p_node;
	m_size--;
}

template <typename T>
void BinarySearchTree<T>::erase_OneChild(Node<T>* p_node) {
	if (p_node->m_parent == nullptr) {				//if root
		m_root = (p_node->m_greater != nullptr ? p_node->m_greater : p_node->m_lesser);
		m_root->m_parent = nullptr;
		delete p_node;
		m_size--;

		return;
	}

	//Determine link direction, and link from node's parent to node's child
	(p_node->m_parent->m_greater == p_node ? p_node->m_parent->m_greater : p_node->m_parent->m_lesser) = (p_node->m_greater != nullptr ? p_node->m_greater : p_node->m_lesser);
	//Set node's child's parent to node's parent
	(p_node->m_greater != nullptr ? p_node->m_greater->m_parent : p_node->m_lesser->m_parent) = p_node->m_parent;

	delete p_node;
	m_size--;
}

template <typename T>
void BinarySearchTree<T>::erase_TwoChildren(Node<T>* p_node) {
	if (p_node->m_parent == nullptr) {				//if root
		Node<T> *current = p_node->m_greater, *previous = p_node;

		while (current->m_lesser != nullptr) {
			previous = current;
			current = current->m_lesser;
		}

		p_node->m_data = current->m_data;

		(previous == p_node ? previous->m_greater : previous->m_lesser) = current->m_greater;

		if (current->m_greater)
			current->m_greater->m_parent = previous;

		delete current;
	}
	else if (p_node->m_parent->m_greater == p_node) {
		Node<T> *current = p_node->m_lesser, *previous = p_node;

		while (current->m_greater != nullptr) {
			previous = current;
			current = current->m_greater;
		}

		p_node->m_data = current->m_data;

		(previous == p_node ? previous->m_lesser : previous->m_greater) = current->m_lesser;

		if (current->m_greater)
			current->m_lesser->m_parent = previous;

		delete current;
	}
	else if (p_node->m_parent->m_lesser == p_node) {
		Node<T> *current = p_node->m_greater, *previous = p_node;

		while (current->m_lesser != nullptr) {
			previous = current;
			current = current->m_lesser;
		}

		p_node->m_data = current->m_data;

		(previous == p_node ? previous->m_greater : previous->m_lesser) = current->m_greater;

		if (current->m_greater)
			current->m_greater->m_parent = previous;

		delete current;
	}

	m_size--;
}

template <typename T>
void BinarySearchTree<T>::traversal_pre_order(Node<T> *p_node) {
	//print -> left -> right
	m_valueList.push_back(p_node->m_data);

	if (p_node->m_lesser)
		traversal_pre_order(p_node->m_lesser);

	if (p_node->m_greater)
		traversal_pre_order(p_node->m_greater);
}

template <typename T>
void BinarySearchTree<T>::traversal_in_order(Node<T> *p_node) {
	//left -> print -> right
	if (p_node->m_lesser)
		traversal_in_order(p_node->m_lesser);

	m_valueList.push_back(p_node->m_data);

	if (p_node->m_greater)
		traversal_in_order(p_node->m_greater);
}

template <typename T>
void BinarySearchTree<T>::traversal_post_order(Node<T> *p_node) {
	//left -> right -> print
	if (p_node->m_lesser)
		traversal_post_order(p_node->m_lesser);

	if (p_node->m_greater)
		traversal_post_order(p_node->m_greater);

	m_valueList.push_back(p_node->m_data);
}