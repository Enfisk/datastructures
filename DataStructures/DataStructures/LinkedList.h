#pragma once
#include <initializer_list>

template <typename T> class LinkedList {

	template <typename U>
	struct Node {
		Node(U p_data, Node<U> *p_next) { data = p_data; next = p_next; }

		U data;
		Node<U> *next = nullptr;
	};

public:
	LinkedList();
	LinkedList(std::initializer_list<T> &p_list);
	//LinkedList(const LinkedList &p_other);
	~LinkedList();

	void push_front(const T &p_value);
	void push_back(const T &p_value);

	void pop_front();
	void pop_back();

	T* operator[](const unsigned int &p_position) const;

	void erase(const unsigned int &p_position);
	int search(const T &p_value) const;

	unsigned int size() const;
private:
	unsigned int m_size;
	Node<T> *m_head;
};

/********METHODS*********
*************************
*************************
************************/

template <typename T>
LinkedList<T>::LinkedList() {
	m_head = nullptr;
	m_size = 0;
}

template <typename T>
LinkedList<T>::LinkedList(std::initializer_list<T> &p_list) {
	m_head = nullptr;
	Node<T> *current = m_head;

	for (auto i = p_list.begin(); i != p_list.end(); ++i) {
		if (m_head == nullptr) {
			m_head = new Node<T>(*i, nullptr);
			current = m_head;
			m_size++;
			continue;
		}
		current->next = new Node<T>(*i, current);
		current = current->next;
		m_size++;
	}

	current->next = nullptr;
}

//template <typename T>
//LinkedList<T>::LinkedList(const LinkedList &p_other) {
//	m_head = p_other.m_head;
//	m_size = p_other.m_size;
//}

template <typename T>
LinkedList<T>::~LinkedList() {
	while (m_head != nullptr) {
		pop_back();
	}

	//if (m_head != nullptr) {
	//	Node<T> *current = m_head, *temp = nullptr;

	//	while (current->next != nullptr) {
	//		temp = current->next;
	//		delete current;
	//		current = temp;
	//	}
	//}
}

template <typename T>
void LinkedList<T>::push_front(const T &p_value) {
	m_head = new Node<T>(p_value, m_head);
	m_size++;
}

template <typename T>
void LinkedList<T>::push_back(const T &p_value) {
	if (m_head == nullptr) {	//If empty
		m_head = new Node<T>(p_value, nullptr);
		m_size++;
	}
	else {		//Find last
		Node<T> *current = m_head;
		while (current->next != nullptr) {
			current = current->next;
		}

		current->next = new Node<T>(p_value, nullptr);
		m_size++;
	}
}

template <typename T>
void LinkedList<T>::pop_front() {
	if (m_head != nullptr) {
		Node<T> *next = m_head->next;
		delete m_head;
		m_head = next;
		m_size--;
	}
}

template <typename T>
void LinkedList<T>::pop_back() {
	if (m_head == nullptr) {
		return;
	}
	else {
		Node<T> *current = m_head, *previous = nullptr;
		while (current->next != nullptr) {
			previous = current;
			current = current->next;
		}

		if (current == m_head) {
			delete current;
			m_head = nullptr;
			m_size--;
			return;
		}

		delete current;
		current = nullptr;
		previous->next = nullptr;
		m_size--;
	}
}

template <typename T>
T* LinkedList<T>::operator[](const unsigned int &p_position) const {		//Has to be pointer in order to return nullptr
	if (m_head != nullptr) {
		if (p_position >= m_size) {
			std::cerr << "out of range" << std::endl;
			return nullptr;
		}

		Node<T> *current = m_head;
		for (unsigned int i = 0; i < p_position; ++i) {
			if (current->next == nullptr) {
				std::cerr << "out of range" << std::endl;
				return nullptr;
			}

			current = current->next;
		}

		return &current->data;
	}
	else {
		return nullptr;
	}
}

template <typename T>
void LinkedList<T>::erase(const unsigned int &p_position) {
	if (m_head != nullptr) {
		if (p_position > m_size) {
			std::cerr << "out of range" << std::endl;
			return;
		}

		if (m_size == 1) {
			delete m_head;
			m_head = nullptr;
			m_size--;
			return;
		}

		Node<T> *current = m_head, *previous = m_head;
		for (unsigned int i = 0; i < p_position; ++i) {
			previous = current;
			current = current->next;
		}

		previous->next = current->next;
		delete current;
		m_size--;
	}
}

template <typename T>
int LinkedList<T>::search(const T &p_value) const {
	if (m_head != nullptr) {
		Node<T> *current = m_head;

		for (unsigned int i = 0; i < m_size; ++i) {
			if (current->data == p_value) {
				return i;
			}
			current = current->next;
		}
	}

	return -1;		//Can't find it
}

template <typename T>
unsigned int LinkedList<T>::size() const {
	return m_size;
}