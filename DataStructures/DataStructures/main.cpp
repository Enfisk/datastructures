#include <iostream>
#include <initializer_list>
#include <random>
#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "vld.h"

namespace tests {
	void linkedlist_tests() {
		{
			LinkedList<int> list = std::initializer_list < int > { 1, 2, 3, 4, 5, 6, 7 };
			std::cout << list.size() << " " << *list[0] << " " << *list[list.size() - 1] << std::endl;

			list.pop_back();
			list.pop_front();
			std::cout << list.size() << " " << *list[0] << " " << *list[list.size() - 1] << std::endl;

			list.push_back(0);
			list.push_front(8);
			std::cout << list.size() << " " << *list[0] << " " << *list[list.size() - 1] << std::endl;

			list.erase(4);
			std::cout << list.size() << " " << *list[0] << " " << *list[list.size() - 1] << std::endl;

			list.erase(12); //should error
			std::cout << std::endl;
		}
		//{
		//	LinkedList<std::string> list = std::initializer_list < std::string > {"a", "b", "c", "hej", "ExampleText", "HYES"};
		//	std::cout << list.size() << " " << list[0] << " " << list[list.size() - 1] << std::endl;

		//	list.pop_back();
		//	list.pop_front();
		//	std::cout << list.size() << " " << (*list[0]) << " " << list[list.size() - 1] << std::endl;

		//	list.push_back("push_back");
		//	list.push_front("push_front");
		//	std::cout << list.size() << " " << list[0] << " " << list[list.size() - 1] << std::endl;

		//	list.erase(list.search("hej"));
		//	std::cout << list.size() << " " << list[0] << " " << list[list.size() - 1] << std::endl;

		//	std::cout << std::endl;
		//}
	}

	void bst_tests() {
		//Trying to use <random> while erasing seems to crash the program, and only rarely does it actually get through without crashing.
		//Manually inserting and erasing values work, from what I can see.
		//BinarySearchTree<int> tree;


		//BinarySearchTree<int> tree = std::initializer_list < int > {3, 8, 4, 7, 2, 9, 1};
		BinarySearchTree<int> tree = std::initializer_list < int > { 217, 862, 961, 456, 815, 551, 378, 350, 958, 921, 100, 120, 749, 814, 473, 163, 565, 943, 274, 0, 963};
		std::cout << "Tree size: " << tree.size() << std::endl;

		tree.insert(5);
		tree.insert(3);
		tree.insert(2);
		tree.insert(20);
		tree.insert(22);
		tree.insert(7);
		tree.insert(15);
		tree.insert(17);
		tree.insert(13);

		tree.traversal_pre_order();
		tree.traversal_in_order();
		tree.traversal_post_order();

		tree.erase(2);
		tree.erase(20);
		tree.erase(17);

		tree.erase(163);
		tree.erase(120);
		tree.erase(217);		//Duplicate #1
		tree.erase(814);
		tree.erase(921);
		tree.erase(0);
		tree.erase(943);
		tree.erase(378);
		tree.erase(473);
		tree.erase(565);
		tree.erase(551);
		tree.erase(815);
		tree.erase(456);		//Duplicate #2
		tree.erase(862);
		tree.erase(217);		//Duplicate #1
		tree.erase(456);		//Duplicate #2
		tree.erase(749);
		tree.erase(100);

		//BinarySearchTree<int> tree;

		//std::random_device rd;
		//std::mt19937 mt(rd());
		//std::uniform_int_distribution<int> distr(0, 1000);
		//int randomNumber = 0;

		//for (int i = 0; i < 1000; ++i) {
		//	randomNumber = distr(mt);
		//	tree.insert(randomNumber);
		//}

		//std::cout << tree.size() << std::endl;

		//while (tree.size() > 0) {
		//	tree.erase(distr(mt));
		//}

		//std::uniform_int_distribution<int> loopRandomer(0, tree.size());
		//int loops = loopRandomer(mt);
		//for (int i = 0; i < loops; ++i) {
		//	if (tree.size() == 0) {
		//		break;
		//	}
		//	randomNumber = distr(mt);
		//	tree.erase(randomNumber);
		//}
		//
		std::cout << " Size: " << tree.size() << std::endl;
		tree.clear();
	}
}

int main() {
	tests::linkedlist_tests();
	tests::bst_tests();
	
	return 0;
}